<?php
// Require Library
require_once 'framework/medoo/medoo.php';

// Input
if(isset($_POST['btnSubmit']))
{
    $author = $_POST['author'];
    $database_host = $_POST['database_host'];
    $database_name = $_POST['database_name'];
    $database_user = $_POST['database_user'];
    $database_pass = $_POST['database_pass'];
} else {
    header("Location: index.php");
    die();
}
//
$database_config = [
    'database_type' => 'mysql',
    'database_name' => $database_name,
    'server'        => $database_host,
    'username'      => $database_user,
    'password'      => $database_pass,
    'charset'       => 'utf8',
    'port'          => 3306
];
$database = new medoo($database_config);

// First Query
$tables = $database->query("SELECT * FROM information_schema.tables WHERE table_schema = '$database_name';")->fetchAll();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Data Dictionary - <?php echo $database_name ?></title>

        <link href="https://fonts.googleapis.com/css?family=Athiti:200,300,400,500,600,700&subset=thai" rel="stylesheet">
        <link href="http://cdis.nsru.ac.th/cdn/fonts/thsarabunnew.css" rel="stylesheet">
        <link href="css/gend.min.css" rel="stylesheet">

    </head>
    <body>

        <div class="container">

        <?php
            foreach($tables as $table)
            {
                $table_name = $table['TABLE_NAME'];
                $fields = $database->query("SELECT * FROM information_schema.columns WHERE table_schema = '$database_name' AND table_name = '$table_name';")->fetchAll();
                ?>
                <div style="text-align:right;width:100%;">
                    <h1><?php echo $table_name ?></h1>
                    <p><?php echo $table['TABLE_COMMENT'] ?></p>
                </div>
                <br>
                <table cellspacing="0">
                    <tr class="table-head">
                        <td>Key</td>
                        <td>Name</td>
                        <td>Caption</td>
                        <td>Allow Null</td>
                        <td>Data Type</td>
                        <td>Length</td>
                    </tr>
                    <?php
                    foreach($fields as $field)
                    {
                        ?>
                        <tr>
                            <td style="text-align:center;"><?php echo $field['COLUMN_KEY'] ?></td>
                            <td><?php echo $field['COLUMN_NAME'] ?></td>
                            <td><?php echo $field['COLUMN_COMMENT'] ?></td>
                            <td style="text-align:center;"><?php echo $field['IS_NULLABLE'] ?></td>
                            <td style="text-align:center;"><?php echo $field['DATA_TYPE'] ?></td>
                            <td style="text-align:center;"><?php echo $field['CHARACTER_MAXIMUM_LENGTH'] ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <div style="page-break-before: always;"></div>
                <?php
            }
        ?>
        </div>
    </body>
</html>
