<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>

        <link href="https://fonts.googleapis.com/css?family=Athiti:200,300,400,500,600,700&subset=thai" rel="stylesheet">
        <link href="http://cdis.nsru.ac.th/cdn/fonts/thsarabunnew.css" rel="stylesheet">
        <link href="css/gend.min.css" rel="stylesheet">

    </head>
    <body>
        <form action="render.php" method="post">
            <div id="config_form">
                <table>
                    <tr>
                        <td>
                            Hostname
                        </td>
                        <td>
                            <input type="text" name="database_host" value="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Database
                        </td>
                        <td>
                            <input type="text" name="database_name" value="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Username
                        </td>
                        <td>
                            <input type="text" name="database_user" value="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password
                        </td>
                        <td>
                            <input type="text" name="database_pass" value="">
                        </td>
                    </tr>
                </table>
                <br>

                <table>
                    <tr>
                        <td>
                            Author
                        </td>
                        <td>
                            <input type="text" name="author" value="">
                        </td>
                    </tr>
                </table>
                <br>

                <input class="btn_submit" type="submit" name="btnSubmit" value="Render">
            </div>
        </form>
    </body>
</html>
